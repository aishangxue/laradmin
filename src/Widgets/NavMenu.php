<?php

namespace SC\Admin\Widgets;

use Closure;


class NavMenu extends Widget
{
    protected $view = 'admin::widgets.navmenu';

    protected $navItems = [];

    public function addItem($title, $url, $pos=null)
    {
        $pos = \is_null($pos) ? 50 : $pos;
        $item = new Support\NavMenuItem($title, $url, $pos);

        $insPos = 0;
        for ($i = \count($this->navItems) - 1; $i >= 0; $i--) {
            if ($this->navItems[$i]->pos <= $pos) {
                $insPos = $i + 1;
            }
        }

        \array_splice($this->navItems, $insPos, 0, [$item]);
        return $item;
    }

    public function viewCtx()
    {
        return [
            'navItems' => $this->navItems,
        ];
    }
}