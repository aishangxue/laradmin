<?php

namespace SC\Admin\Widgets;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\HtmlString;


abstract class Widget implements Htmlable
{
    protected $view = null;
    protected $variation = '';

    protected static $elIdCounter = 0;
    protected $elId = null;

    public function __construct($variation='')
    {
        $this->$variation = $variation;
    }

    public function elId()
    {
        if (is_null($this->elId)) {
            $prefix = strtolower(array_slice(explode('\\', get_class($this)), -1, 1)[0]) . '-';
            self::$elIdCounter += 1;
            $this->elId = $prefix . self::$elIdCounter;
        }

        return $this->elId;
    }

    public function toHtml()
    {
        return $this->render();
    }

    public function render()
    {
        return view($this->view(), $this->viewCtx())->render();
    }

    public function view()
    {
        if (!$this->view) {
            throw new \Exception('view 为空');
        }
        return $this->view;
    }

    abstract public function viewCtx();
}