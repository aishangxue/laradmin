<?php

use Illuminate\Routing\Router;


Admin::registerRoutes();

Route::group([
    'prefix'        => 'admin',
    'namespace'     => '\App\Admin\Controllers',
    'middleware'    => ['web', 'admin'],
    'as'            => 'admin.',
], function (Router $router) {
    //Admin::CRUD('ResourceController');
});
