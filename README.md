# laravel 后端管理扩展

## 开发环境

在 composer.json 中配置本地代码路径

```json
{
    "repositories": [
        {
            "type": "path",
            "url": "../laradmin",
            "options": {
                "symlink": true
            }
        }
    ]
}
```

然后执行 require 命令

```sh
composer require sc/laradmin
```

## 生产环境

在 composer.json 中配置git路径

```json
"repositories": [
    {
        "type":"package",
        "package": {
            "name": "swc/laradmin",
            "version":"master",
            "source": {
                "url": "git@gitee.com:aishangxue/laradmin.git",
                "type": "git",
                "reference":"master"
            },
            "autoload": {
                "psr-4": {
                    "SC\\Admin\\": "src"
                },
                "files": [
                    "src/helpers.php"
                ]
            }
        }
    }
],
```

然后执行 require 命令

```sh
composer require sc/laradmin
```