@extends('admin::layouts.base')

@section('css')
<style>
    body > .grid {
        height: 100%;
    }
    .column {
        max-width: 450px;
    }
</style>
@endsection

@section('body')
<div class="ui middle aligned center aligned grid">
    <div class="column">
        <form action="/login" method="POST" class="ui large form @if($errors->any()) error @endif">
            {{csrf_field()}}
            <input type="checkbox" name="remember" checked style="display:none">
            <div class="ui stacked segment">
                <div class="field @if($errors->has('email')) error @endif">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="email" placeholder="用户名" value="{{old('email')}}">
                    </div>
                </div>
                <div class="field @if($errors->has('password')) error @endif">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="密码" value="{{old('password')}}">
                    </div>
                </div>
                <button class="ui fluid large teal submit button" type="submit">登录</button>
            </div>
            <ui class="ui error message">
            @if($errors->any())
            <ul class="list">
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
            @endif
            </ui>
        </form>
    </div>
</div>
@endsection
