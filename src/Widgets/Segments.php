<?php

namespace SC\Admin\Widgets;


class Segments extends Container
{
    public function render()
    {
        return '<div class="ui segments">'. $this->renderChilds() .'</div>';
    }
}