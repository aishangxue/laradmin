<?php

namespace SC\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SC\Admin\Support\Canvas;
use SC\Admin\Widgets\Panel;
use SC\Admin\Widgets\Form;
use SC\Admin\Widgets\Grid;


class AdminController extends Controller
{
    static public $model = null; //模型类
    protected $canvas = null;

    public function __construct()
    {
        $canvas = new Canvas();

        $resource = (new $this::$model)->getTable();
        $action = collect(\explode('.', request()->route()->getName()))->last();
        if ($action == 'index') {
            $canvas->title("{$resource} 列表")
                ->breadcrumb(function($breadcrumb) use ($resource) {
                    $breadcrumb->section($resource);
                });
        } else if ($action == 'show') {
            $canvas->title("{$resource} 详情")
                ->breadcrumb(function($breadcrumb) use ($resource) {
                    $id = collect(\explode('/', request()->path()))->last();
                    $breadcrumb->section($resource, "/admin/{$resource}")
                        ->section("详情({$id})");
                });
        } else if ($action == 'edit') {
            $canvas->title("{$resource} 编辑")
                ->breadcrumb(function($breadcrumb) use ($resource) {
                    $id = \explode('/', request()->path())[2];
                    $breadcrumb->section($resource, "/admin/{$resource}")
                        ->section("编辑({$id})");
                });
        } else if ($action == 'create') {
            $canvas->title("{$resource} 编辑")
                ->breadcrumb(function($breadcrumb) use ($resource) {
                    $breadcrumb->section($resource, "/admin/{$resource}")
                        ->section('创建');
                });
        }
        $this->canvas = $canvas;
    }

    public function index()
    {
        return $this->canvas->grid($this::$model, [$this, 'grid']);
    }

    public function show($id)
    {
        return $this->canvas->display($this::$model, $id, [$this, 'display']);
    }

    public function create()
    {
        return $this->canvas->form($this::$model, null, [$this, 'form']);
    }

    public function edit($id)
    {
        return $this->canvas->form($this::$model, $id, [$this, 'form']);
    }

    public function store(Request $request)
    {
        $inst = $this::$model::create($request->all());

        return response()->json([
            'id' => $inst->id,
        ]);
    }

    public function update(Request $request, $id)
    {
        $this::$model::where('id', $id)->update($request->all());
        return response()->json([
            'id' => $id,
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this::$model::destroy($id);
        return response()->json([
            'id' => $id,
        ]);
    }

    public function grid(Grid $grid)
    {
        $this->panel($grid);
    }

    public function form(Form $form)
    {
        $this->panel($form);
    }

    public function display(Form $form)
    {
        $this->panel($form);
    }

    public function panel(Panel $panel)
    {
        //
    }
}
