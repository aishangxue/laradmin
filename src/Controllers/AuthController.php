<?php

namespace SC\Admin\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class AuthController
{
    use AuthenticatesUsers;
    protected $redirectTo = '/admin';

    public function login()
    {
        if (Auth::user()) {
            return redirect($this->redirectTo);
        }

        return view('admin::login');
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        if($this->hasTooManyLoginAttempts($request)){
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);
        if ($this->guard()->attempt($credentials, $request->has('remember'))){
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('admin.login');
    }
}
