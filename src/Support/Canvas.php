<?php

namespace SC\Admin\Support;

use Closure;
use Illuminate\Contracts\Support\Renderable;
use SC\Admin\Widgets\Container;
use SC\Admin\Widgets\Breadcrumb;
use SC\Admin\Widgets\Traits;

class Canvas extends Container implements Renderable
{
    use Traits\Panelable;

    protected $view = 'admin::canvas';
    protected $title = '';

    protected $breadcrumb = null;

    public function title($title)
    {
        $this->title = $title;
        return $this;
    }

    public function breadcrumb(Closure $closure)
    {
        $this->breadcrumb = new Breadcrumb();
        $closure($this->breadcrumb);
        return $this;
    }

    public function viewCtx()
    {
        return [
            'title' => $this->title,
            'breadcrumb' => $this->breadcrumb,
            'childs' => $this->childs,
        ];
    }
}
