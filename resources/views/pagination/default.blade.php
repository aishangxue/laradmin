<div class="ui pagination menu floated right">
    @foreach ($elements as $element)
        @if (is_string($element))
        <div class="disabled item">
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <a class="active item">{{ $page }}</a>
                @else
                    <a href="{{ $url }}" class="item">{{ $page }}</a>
                @endif
            @endforeach
        @endif
    @endforeach
</div>
