@extends('admin::layouts.app')

@section('content')
    @if($breadcrumb)
        {{$breadcrumb}}
    @endif
    @foreach($childs as $child)
        {{$child}}
    @endforeach
@endsection

@push('js')
<script>
const delayRedirect = (url, delay=1500) => {
    setTimeout(() => {
        window.location.replace(url)
    }, delay)
}
</script>
@endpush
