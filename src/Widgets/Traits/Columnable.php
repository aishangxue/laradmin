<?php

namespace SC\Admin\Widgets\Traits;

use Closure;
use SC\Admin\Widgets\Column;


trait Columnable
{
    public function column($width, Closure $closure)
    {
        $column = new Column($width);
        $closure($column);
        return $this->addChild($column);
    }
}