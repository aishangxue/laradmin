@extends('admin::layouts.base')


@section('css')
<style>
[v-cloak] {
    display: none;
}

.inverted.menu {
    border-radius: 0;
}

.ui.footer.segment {
    margin: 3em 0 0;
    padding: 3em 0;
}
</style>
@endsection


@section('body')

{{ \Admin::navMenu() }}

<div class="ui main container">
    @section('content')
    @show
</div>

@include('partials.footer')

@endsection