<div class="ui segment basic">
    <div class="ui breadcrumb">
        <a href="/admin" class="section">首页</a>
        @foreach($sections as $section)
            <span class="divider">/</span>
            @if($section[1])
                <a href="{{$section[1]}}" class="section">{{ucfirst($section[0])}}</a>
            @else
                <span class="active section">{{ucfirst($section[0])}}</span>
            @endif
        @endforeach
    </div>
</div>