<?php

namespace SC\Admin\Widgets;


abstract class Container extends Widget
{
    protected $childs = [];

    public function addChild($widget, $append=true) {
        $this->childs[] = $widget;
        return $this;
    }

    public function renderChilds()
    {
        $rv = [];
        foreach($this->childs as $child){
            $rv[] = $child->render();
        }

        return \implode('', $rv);
    }
}