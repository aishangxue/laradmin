<?php

namespace SC\Admin\Controllers;

use SC\Admin\Widgets\Panel;


class SettingController extends AdminController
{
    static public $model = \SC\Admin\Model\Setting::class;

    public function panel(Panel $panel)
    {
        $panel->pk();
        $panel->string('key');
        $panel->string('value');
    }
}