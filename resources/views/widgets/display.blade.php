<div class="ui segments" id="{{$elId}}" v-cloak>
    <div class="ui segment secondary clearing">
        <span class="ui header" v-text="`${resource} 详情`"></span>
        <div class="ui icon buttons right floated">
            <a :href="`/admin/${resource}`" class="ui button small teal" title="列表">
                <i class="icon list"></i>
            </a>
            <a :href="`/admin/${resource}/${id}/edit`" class="ui button small blue" title="编辑">
                <i class="icon edit outline"></i>
            </a>
            <a @click.prevent="handleDelete" class="ui button small red" title="删除">
                <i class="icon trash alternate outline"></i>
            </a>
        </div>
    </div>
    <div class="ui segment clearing">
        <form class="ui form">
            <div v-for="(field, index) in fields" class="field" :class="{'inline': field.type === 'pk'}">
                <label v-text="field.label"></label>
                <template v-if="field.type=='select' && field.options">
                    <input :value="_.get(field.options, values[index], values[index]) + ' / ' + values[index]" type="text" disabled>
                </template>
                <template v-else-if="field.type=='image'">
                    <input :value="values[index]" type="text" disabled>
                    <image :src="values[index]" v-show="_.startsWith(values[index], 'http')" class="ui bordered image" :class="field.formSize">
                </template>
                <template v-else>
                    <input :value="values[index]" type="text" disabled>
                </template>
            </div>
        </form>
    </div>
</div>

@push('js')
<script>
/**
 * {{$elId}}
 */
$(function(){
    const elId = {!! json_encode($elId) !!}
    const vueData = {
        actions: {!! json_encode($actions) !!},
        resource: {!! json_encode($resource) !!},
        id: {!! json_encode($id) !!},
        fields: {!! json_encode($fields) !!},
        values: {!! json_encode($values) !!},
    }
    new Vue({
        el: '#' + elId,
        data: vueData,
        methods: {
            handleDelete() {
                swal(`确定要删除记录 id=${this.id} 吗？`, {
                    dangerMode: true,
                    buttons: true,
                }).then((confirm) => {
                    if (!confirm) {
                        return;
                    }

                    axios.delete(`/admin/${this.resource}/${this.id}`)
                    .then(()=>{
                        toastr.info(`删除成功, id: ${this.id}`)

                        delayRedirect(`/admin/${this.resource}`)
                    }).catch(error => {
                        console.error(error)
                        toastr.error(`删除失败: ${error}`)
                    })
                }) 
            },
        },
    })
})
</script>
@endpush