<?php

namespace SC\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class MakeControllerCommand extends Command
{
    protected $signature = 'admin:controller {name} {--model=}';
    protected $description = 'Create new controller for admin';

    public function handle(Filesystem $fs)
    {
        $controller = $this->argument('name');
        $model = $this->option('model') ?: '';
        if ($model && !\Str::endsWith($model, '::class')) {
            $model .= '::class';
        }

        $stubPath = __DIR__ . '/../../resources/stubs/Controllers/Controller.tpl';
        $controllerPath = \admin_path("Controllers/{$controller}.php");
        if ($fs->exists($controllerPath)) {
            $this->error('Controller already exists.');
            return;
        }


        $content = $fs->get($stubPath);
        $content = \str_replace('{{controller}}', $controller, $content);
        $content = \str_replace('{{model}}', $model, $content);
        $fs->put($controllerPath, $content);

        $this->info("Create controller file: ${controllerPath}");
        $this->info("NEXT: add controller at router file.");
    }
}