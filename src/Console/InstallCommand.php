<?php

namespace SC\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class InstallCommand extends Command
{
    protected $signature = 'admin:install';
    protected $description = 'Install admin resource';

    protected $files = [
        'bootstrap.php',
        'routes.php',
        'Controllers/HomeController.php',
    ];

    protected $fs = null;

    public function handle(Filesystem $fs)
    {
        $this->fs = $fs;
        $this->initDatabase();
        $this->initAdminFiles();
    }

    protected function initDatabase()
    {
        $this->call('migrate');
    }

    protected function initAdminFiles()
    {
        if (\is_dir(\admin_path())) {
            $this->error(\admin_path()."directory already exists!");
            return;
        }

        $this->mkdir();
        $this->mkdir('Controllers');
        $this->info('Admin directory was created');

        $this->mv($this->files);
    }

    protected function stub_path($file)
    {
        return __DIR__."/../../resources/stubs/{$file}";
    }

    protected function mkdir($path='')
    {
        $this->fs->makeDirectory(\admin_path($path), 0755, true, true);
    }

    protected function mv($files) {
        foreach($files as $file){
            $fromPath = $this->stub_path($file);
            $toPath = \admin_path($file);
            $this->fs->copy($fromPath, $toPath);
        }
    }
}