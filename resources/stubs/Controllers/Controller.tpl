<?php

namespace App\Admin\Controllers;

use SC\Admin\Controllers\AdminController;
use SC\Admin\Widgets\Panel;


class {{controller}} extends AdminController
{
    public static $model = {{model}};

    public function panel(Panel $panel)
    {
        $panel->pk();
        //todo: add more fields, like ...
        //$panel->string('name);
    }
}
