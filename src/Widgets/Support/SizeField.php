<?php

namespace SC\Admin\Widgets\Support;

use Closure;


class SizeField extends Field
{
    protected $gridSize = 'small';
    protected $formSize = 'medium';

    public function size($size)
    {
        $this->gridSize = $size;
        $this->formSize = $size;
        return $this;
    }

    public function gridSize($size)
    {
        $this->gridSize = $size;
        return $this;
    }

    public function formSize($size)
    {
        $this->formSize = $size;
        return $this;
    }

    public function meta()
    {
        return array_merge(parent::meta(), [
            'gridSize' => $this->gridSize,
            'formSize' => $this->formSize,
        ]);
    }
}