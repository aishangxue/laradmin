<?php

namespace SC\Admin\Widgets\Support;

use Closure;


class OptionsField extends Field
{
    protected $options = null;

    public function options($options)
    {
        $this->options = $options;
        return $this;
    }

    public function meta()
    {
        return array_merge(parent::meta(), [
            'options' => $this->options,
        ]);
    }
}