<?php

namespace SC\Admin\Widgets;


class Column extends Container
{
    use Traits\Segmentable;
    use Traits\Segmentsable;

    protected $width = 8;
    protected static $columnWords = [
        'zero',
        'one',
        'two',
        'three',
        'four',
        'five',
        'six',
        'eight',
        'nine',
        'ten',
        'eleven',
        'twelve',
        'thirteen',
        'fourteen',
        'fifteen',
        'sixteen',
    ];

    public function __construct($width=8)
    {
        $this->width = $width < 1 ? round($width * 16) : $width;
    }

    public function render()
    {
        $result = "<div class=\"{$this->columnWords[$this->width]} wide column\">". $this->renderChilds() .'</div>';
    }
}