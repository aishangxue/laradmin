<?php

namespace SC\Admin\Widgets\Support;

use Closure;


class NavMenuItem
{
    public $title = null;
    public $url = null;
    public $pos = null;
    public $subItems = [];

    public function __construct($title, $url, $pos=null)
    {
        $this->title = $title;
        $this->url = $url;
        $this->pos = $pos;
    }

    public function addSub($title, $url)
    {
        $this->subItems[] = NavMenuItem($title, $url);
        return $this;
    }
}