<?php

namespace SC\Admin\Widgets\Traits;

use Closure;
use SC\Admin\Widgets\Grid;
use SC\Admin\Widgets\Form;


trait Panelable
{
    public function grid($model, $callable)
    {
        $grid = new Grid($model);
        $callable($grid);
        return $this->addChild($grid);
    }

    public function display($model, $id, $callable)
    {
        $form = new Form($model, $id, true);
        $callable($form);
        return $this->addChild($form);
    }

    public function form($model, $id, $callable)
    {
        $form = new Form($model, $id);
        $callable($form);
        return $this->addChild($form);
    }
}