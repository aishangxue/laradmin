<?php

namespace SC\Admin\Widgets\Traits;

use Closure;
use SC\Admin\Widgets\Segment;


trait Segmentable
{
    public function segment($variation, $closure)
    {
        $segment = new Segment($variation);
        $closure($segment);
        return $this->addChild($segment);
    }
}

trait Segmentsable
{
    public function segments($variation, $closure)
    {
        $segments = new Segments($variation);
        $closure($segments);
        return $this->addChild($segments);
    }
}
