<nav class="ui inverted menu">
    <div class="ui container">
        <a href="/admin" class="item">首页</a>
        @foreach($navItems as $item)
        <a href="{{$item->url}}" class="item" pos="{{$item->pos}}">{{$item->title}}</a>
        @endforeach
        <!--
        <a href="#" class="item">Menu2</a>
        <div class="ui simple dropdown item">
            Dropdown <i class="dropdown icon"></i>
            <div class="menu">
                <a href="#" class="item">Link1</a>
                <a href="#" class="item">Link2</a>
                <div class="divider"></div>
                <div class="header">Header</div>
                <a href="#" class="item">Link3</a>
            </div>
        </div>
        -->
    </div>
</nav>