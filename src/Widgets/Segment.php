<?php

namespace SC\Admin\Widgets;


class Segment extends Container
{
    use Traits\Segmentable;

    public function render()
    {
        return "<div class=\"ui {$this->variation} segment\">". $this->renderChilds() .'</div>';
    }
}