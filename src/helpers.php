<?php

if (!function_exists('admin_path')) {
    function admin_path($path='')
    {
        return app_path('Admin') . ($path ? '/'.$path : '');
    }
}