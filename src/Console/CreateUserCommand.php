<?php

namespace SC\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;


class CreateUserCommand extends Command
{
    protected $signature = 'admin:createuser {email} {password} {--name=}';
    protected $description = 'Create admin user';

    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');
        $name = $this->option('name') ?: $email;

        \App\User::create([
            'email' => $email,
            'password' => Hash::make($password),
            'name' => $name,
        ]);

        $this->info("创建账号成功: $email");
    }
}