<?php

namespace SC\Admin\Widgets\Traits;


trait HasLabel
{
    protected $label = null;

    public function label($label=null)
    {
        if(is_null($label)){
            return $this->label;
        } else {
            $this->label = $label;
            return $this;
        }
    }
}