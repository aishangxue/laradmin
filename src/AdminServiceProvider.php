<?php

namespace SC\Admin;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;


class AdminServiceProvider extends ServiceProvider
{
    protected $commands = [
        Console\InstallCommand::class,
        Console\CreateUserCommand::class,
        Console\MakeControllerCommand::class,
    ];

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'admin');

        if (\file_exists($routes = admin_path('routes.php'))) {
            $this->loadRoutesFrom($routes);
        }

        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../publishable/config' => config_path()]);
            $this->publishes([__DIR__.'/../publishable/assets' => public_path('vendor/admin')]);
        }

        $this->loadMigrationsFrom(realpath(__DIR__.'/../migrations'));

    }

    public function register()
    {
        AliasLoader::getInstance()->alias('Admin', '\SC\Admin\Facades\Admin');

        $this->registerRouteMiddleware();

        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }

    public function registerRouteMiddleware()
    {
        $routeMiddleware = [
            'admin.auth' => Middleware\Auth::class,
            //'admin.bootstrap' => Middleware\Bootstrap::class,
        ];

        $middlewareGroups = [
            'admin' => [
                'admin.auth',
                //'admin.bootstrap',
            ],
        ];

        foreach($routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }

        foreach($middlewareGroups as $key => $middleware) {
            app('router')->middlewareGroup($key, $middleware);
        }
    }
}
