<div class="ui segments" id="{{$elID}}" v-cloak>
    <div class="ui segment secondary clearing">
        <span class="ui header" v-text="title ? title : `${resource} 列表`"></span>
        <div v-if="searchable" class="ui search" style="display: inline-block; margin-left: 15px;">
            <div class="ui icon input mini">
                <input v-model="q" @keyup.enter="handleSearch" class="prompt" type="text" placeholder="输入 ID 或 where语句">
                <i class="search icon"></i>
            </div>
        </div>
        <div class="ui buttons right floated icon">
            <a :href="`/admin/${resource}/create`" class="ui button blue" title="创建">
                <i class="icon plus"></i>
            </a>
        </div>
    </div>
    <div>
        <table class="ui celled table">
            <thead>
                <tr>
                    <th v-for="field in fields" v-text="field.label"></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(row, index) in values">
                    <template v-for="(item, $colIndex) in row">
                        <td v-if="fields[$colIndex].type == 'select' && fields[$colIndex].options">
                            @{{_.get(fields[$colIndex].options, item, item) + ` / ${item}`}}
                        </td>
                        <td v-else-if="fields[$colIndex].type == 'image'">
                            <image :src="item" class="ui bordered image" :class="fields[$colIndex].gridSize">
                        </td>
                        <td v-else v-text="item"></td>
                    </template>
                    <td>
                        <div class="ui small icon buttons">
                            <a v-if="actions.includes('display')" :href="`/admin/${resource}/${rowIds[index]}`" class="ui button" title="详情">
                                <i class="icon file outline"></i>
                            </a>
                            <a v-if="actions.includes('update')" :href="`/admin/${resource}/${rowIds[index]}/edit`" class="ui button blue" title="编辑">
                                <i class="icon edit outline"></i>
                            </a>
                            <a v-if="actions.includes('delete')" @click.prevent="handleDelete(rowIds[index])" class="ui button red" title="删除">
                                <i class="icon trash alternate outline"></i>    
                            </a>
                        </div>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th :colspan="fields.length + 1">
                        @if($pagination)
                        {{ $pagination->render('admin::pagination.default') }}
                        
                        @php
                            $start = ($pagination->currentPage()-1) * $pagination->perPage() + 1;
                            $end = $start + count($pagination->items()) - 1;
                        @endphp
                        <div class="ui middle aligned">{{ "{$start}-{$end}/{$pagination->total()}" }}</div>
                        @endif
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


@push('js')
<script>

/**
 * {{$elID}}
 */
$(function(){
    const elID = "{{$elID}}"
    const vueData = {
        title: {!! json_encode($title) !!},
        actions: {!! json_encode($actions) !!},
        fields: {!! json_encode($fields) !!},
        values: {!! json_encode($values) !!},
        resource: {!! json_encode($resource) !!},
        rowIds: {!! json_encode($rowIds) !!},
        q: {!! json_encode($q) !!},
        originQ: {!! json_encode($q) !!},
        searchable: {!! json_encode($searchable) !!},
    }

    new Vue({
        el: '#' + elID,
        data: vueData,
        computed: {
        },
        methods: {
            handleSearch(evt){
                if (this.originQ === this.q) {
                    return
                }
                delayRedirect(`/admin/${this.resource}?q=${this.q}`, 0)
            },
            handleDelete(id){
                swal(`确定要删除记录 id=${id} 吗？`, {
                    dangerMode: true,
                    buttons: true,
                }).then((confirm) => {
                    if (!confirm) {
                        return;
                    }

                    axios.delete(`/admin/${this.resource}/${id}`)
                    .then(()=>{
                        toastr.info(`删除成功, id: ${id}`)

                        delayRedirect(`/admin/${this.resource}`)
                    }).catch(error => {
                        console.error(error)
                        toastr.error(`删除失败: ${error}`)
                    })
                })                
            }
        }
    })
});
</script>
@endpush