<?php

namespace SC\Admin\Widgets;

use Clousre;
use Traits\Columnable;


class Row extends Container
{
    use Columnable;

    public function render()
    {
        $result = '<div class="ui row">'. $this->renderChilds() .'</div>';
        return $result;
    }
}