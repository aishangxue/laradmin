<?php

namespace SC\Admin\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth as Authenticate;


class Auth
{
    public function handle($request, Closure $next)
    {
        if (!Authenticate::guest()) {
            return $next($request);
        }

        return redirect()->guest(route('admin.login'));
    }
}