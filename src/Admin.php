<?php

namespace SC\Admin;


class Admin
{
    protected $extCss = [];
    protected $extHeaderJs = [];
    protected $extJs = [];
    protected $navMenu = null;

    protected $registedResources = [];
    protected $cachedSettings = null;

    public function registerRoutes()
    {
        app('router')->group([
            'namespace' => 'SC\Admin\Controllers',
            'as' => 'admin.',
            'middleware' => ['web'],
        ], function ($router) {
            $router->get('/login', 'AuthController@login')->name('login');
            $router->post('/login', 'AuthController@postLogin')->name('postLogin');
            $router->post('/logout', 'AuthController@logout')->name('logout');
        });

        app('router')->group([
            'prefix' => 'admin',
            'namespace' => 'SC\Admin\Controllers',
            'as' => 'admin.',
            'middleware' => ['web', 'admin'],
        ], function ($router) {
            $router->get('/', 'HomeController@index')->name('home');
            \Admin::CRUD('\SC\Admin\Controllers\SettingController', 100);
        });
    }

    public function setting($key, $default=null)
    {
        if (\is_null($this->cachedSettings)) {
            $this->cachedSettings = [];
            foreach(Model\Setting::all() as $setting) {
                $this->cachedSettings[$setting->key] = $setting->value;
            }

            return $this->cachedSettings[$key] ?? $default;
        }
    }

    public function extJs($js=null)
    {
        if (\is_null($js)) {
            return \implode("\n", \array_keys($this->extJs));
        }

        $this->extJs[$js] = 0;
    }

    public function extCss($css=null)
    {
        if (\is_null($css)) {
            return \implode("\n", \array_keys($this->extCss));
        }

        $this->extCss[$css] = 0;
    }

    public function extHeaderJs($js=null)
    {
        if (\is_null($js)) {
            return \implode("\n", \array_keys($this->extHeaderJs));
        }

        $this->extHeaderJs[$js] = 0;
    }

    public function navMenu($menu=null, $pos=null)
    {
        if (\is_null($this->navMenu)) {
            $this->navMenu = new Widgets\NavMenu();
        }

        if (\is_array($menu)) {
            foreach($menu as $title => $url) {
                $this->navMenu->addItem($title, $url, $pos);
            }
        }

        return $this->navMenu;
    }

    public function CRUD($controller, $pos=null)
    {
        $ctlClass = $controller;
        if (!\Str::startsWith($ctlClass, "\\")) {
            $ctlClass = "\App\Admin\Controllers\\" . $ctlClass;
        }
        if (!\class_exists($ctlClass)) {
            throw new \Exception("controller class ${ctlClass} not exists");
        }
        if (!\class_exists($ctlClass::$model)) {
            throw new \Exception("model class ${$ctlClass::$model} in controller ${ctlClass} not exists");
        }

        $resource = (new $ctlClass::$model)->getTable();
        $this->navMenu([\ucfirst($resource) => "/admin/{$resource}"], $pos);
        return \Route::resource($resource, $ctlClass);
    }
}