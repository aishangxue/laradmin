<?php

namespace SC\Admin\Widgets;


class Breadcrumb extends Widget
{
    protected $view = 'admin::widgets.breadcrumb';
    protected $sections = [];

    public function section($title, $url=null)
    {
        $this->sections[] = [$title, $url];
        return $this;
    }

    public function viewCtx()
    {
        return [
            'sections' => $this->sections,
        ];
    }
}